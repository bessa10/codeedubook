<?php

namespace CodeEduBook\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\RepositoryCriteriaInterface;
use CodeEduBook\Repositories\RepositoryRestoreInterface;
use CodePub\Criteria\CriteriaOnlyTrashedInterface;

/**
 * Interface BookRepository
 * @package namespace CodePub\Repositories;
 */
interface BookRepository extends 
	
	RepositoryInterface, 
	RepositoryCriteriaInterface, 
	RepositoryRestoreInterface,
	CriteriaOnlyTrashedInterface
{
    //
}
