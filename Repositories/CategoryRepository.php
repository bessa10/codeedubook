<?php

namespace CodeEduBook\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use CodePub\Criteria\CriteriaOnlyTrashedInterface;

/**
 * Interface CategoryRepository
 * @package namespace CodePub\Repositories;
 */
interface CategoryRepository extends RepositoryInterface, CriteriaOnlyTrashedInterface
{
    public function listsWithMutators($column, $key = null);
}
