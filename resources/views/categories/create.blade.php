@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Nova Categoria</h3>
			{!! Form::open(['route' => 'categories.store', 'class' => 'form', 'method' => 'POST']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Form::label('name', 'Nome:', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
						{!! Form::error('name', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Button::primary('Criar categoria')->submit() !!}
					{!!Html::openFormGroup('name', $errors) !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection
