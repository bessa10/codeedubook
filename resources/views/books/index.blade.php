@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h1>Lista da Livros</h1><br>
			</div>
			<div class="col-md-offset-6 col-md-2">
				{!! Button::primary('Novo Livro')->asLinkTo(route('books.create')) !!}
			</div>
		</div>
		<div class="row">
			{!! Form::model([], ['class' => 'form', 'method' => 'GET']) !!}
				<div class="form-group col-md-6">
					{!! Form::label('search', 'Pesquisar por título ou autor:', ['class'=>'control-label']) !!}
					{!! Form::text('search', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group col-md-2">
					<br>{!! Button::primary('Buscar')->submit() !!}
				</div>
			{!! Form::close() !!}
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<!-- FORMA MAIS PRÁTICA DE FAZER TABELA USANDO O BOOTSTRAPPER -->
				{!! 
					Table::withContents($books->items())->striped()
						->callback('Ações', function($field, $book){
							$linkEdit = route('books.edit', ['book' => $book->id]);
							$linkDestroy = route('books.destroy', ['book' => $book->id]);
							$deleteForm2 = "delete-form2-{$book->id}";
							$form = Form::open(['route' =>
										['books.destroy', 'book' => $book->id],
										'method' => 'DELETE', 'id' => $deleteForm2, 'style' => 'display:none']).
										Form::close();
							$anchorDestroy = Button::danger('Deletar')->asLinkTo($linkDestroy)->addAttributes([
												'onclick' => "event.preventDefault();document.getElementById(\"{$deleteForm2}\").submit();"
											]);			
							return "<ul class=\"list-inline\">".
										"<li>".Button::primary('Editar')->asLinkTo($linkEdit)."</li>".
										"<li>|</li>".
										"<li>".$anchorDestroy."</li>".
									"</ul>".
									$form;
					})
				!!}
			</div>
		</div>
		<div class="col-md-offset-4 col-md-4">
			{{$books->links()}}
		</div>
	</div>
@endsection