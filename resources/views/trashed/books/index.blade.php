@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h1>Lixeira de Livros</h1><br>
			</div>
		</div>
		<div class="row">
			{!! Form::model([], ['class' => 'form', 'method' => 'GET']) !!}
				<div class="form-group col-md-6">
					{!! Form::label('search', 'Pesquisar por título ou autor:', ['class'=>'control-label']) !!}
					{!! Form::text('search', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group col-md-2">
					<br>{!! Button::primary('Buscar')->submit() !!}
				</div>
			{!! Form::close() !!}
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<!-- FORMA MAIS PRÁTICA DE FAZER TABELA USANDO O BOOTSTRAPPER -->
				@if($books->count() > 0)
				{!! 
					Table::withContents($books->items())->striped()
						->callback('Ações', function($field, $book){
							$linkView = route('trashed.books.show', ['book' => $book->id]);
							$linkDestroy = route('books.destroy', ['book' => $book->id]);
							$restoreForm = "restore-form-{$book->id}";
							$form = Form::open(['route' =>
										['trashed.books.update', 'book' => $book->id],
										'method' => 'PUT', 'id' => $restoreForm, 'style' => 'display:none']).
										Form::hidden('redirect_to', URL::previous()).
										Form::close();
							$anchorRestore = Button::danger('Restaurar')->asLinkTo($linkDestroy)->addAttributes([
												'onclick' => "event.preventDefault();document.getElementById(\"{$restoreForm}\").submit();"
											]);			
							return "<ul class=\"list-inline\">".
										"<li>".Button::primary('Visualizar')->asLinkTo($linkView)."</li>".
										"<li>|</li>".
										"<li>".$anchorRestore."</li>".
									"</ul>".
									$form;
					})
				!!}
				@else
				<br>
				<div class="well well-lg text-center">
					<strong>A lixeira está vazia</strong>
				</div>
				@endif
			</div>
		</div>
		<div class="col-md-offset-4 col-md-4">
			{{$books->links()}}
		</div>
	</div>
@endsection