<?php

namespace CodeEduBook\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Bootstrapper\Interfaces\TableInterface;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model implements TableInterface
{
    use FormAccessible;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'title',
    	'subtitle',
    	'price',
        'author_id'
    ];

    public function author()
    {
    	// Relacionamento de muitos para um
    	return $this->belongsTo(\CodeEduUser\Models\User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTrashed();
    }

    public function formCategoriesAttribute()
    {
        return $this->categories->pluck('id')->all();
    }

    public function getTableHeaders()
    {
    	return ['#', 'Titulo', 'Autor', 'Preço'];
    }

    public function getValueForHeader($header)
    {
    	switch ($header) {
    		case '#':
    			return $this->id;
    		
    		case 'Titulo':
    			return $this->title;

    		case 'Autor':
    			return $this->author->name;	

    		case 'Preço':
    			return $this->price;	
    		
    	}
    }



}
