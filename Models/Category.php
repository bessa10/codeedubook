<?php

namespace CodeEduBook\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Bootstrapper\Interfaces\TableInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model implements TableInterface
{
    // importando para usar exclusão lógica
    use SoftDeletes;

    protected $dates = ['deleted_at']; 


    protected $fillable = [
    	'name'
    ];

    // relacionamento de muitos para muitos
    public function books()
    {

        return $this->belongsToMany(Book::class);

    }

    // criando mutator
    public function getNameTrashedAttribute()
    {
        return $this->trashed() ? "{$this->name} (Inativa)": $this->name;
    }


    public function getTableHeaders()
    {
    	return ['#', 'Nome'];
    }

    public function getValueForHeader($header)
    {
    	switch ($header) {
    		case '#':
    			return $this->id;
    		
    		case 'Nome':
    			return $this->name;
    		
    	}
    }


}
