<?php

namespace CodeEduBook\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use CodeEduBook\Repositories\BookRepository;

class BookUpdateRequest extends BookCreateRequest
{

    private $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // pegando o id de book da url
        $id = $this->route('book');

        if ($id == 0) {

            return false;
        }

        // fazendo uma consulta do livro usando o id que vem na URL
        $book = $this->repository->find($id);

        // pegando o id do usuário que está logado
        $idUsuario = \Auth::user()->id;

        /* se o author do book for o mesmo usuário que está logado, então o update será realizado
        if ($book->author_id == $idUsuario) {

            return true;
        
        } else {

            return false;
        } */

        return \Gate::allows('update-book', $book);

        
    }

}
