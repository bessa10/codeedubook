<?php

namespace CodeEduBook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use CodeEduBook\Repositories\BookRepository;
use CodePub\Criteria\FindOnlyTrashedCriteria;

class BooksTrashedController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->repository->pushCriteria(FindOnlyTrashedCriteria::class);

        $this->repository->onlyTrashed();

        $books = $this->repository->paginate(10);

        return view('codeedubook::trashed.books.index', compact('books'));
    }

    public function show($id)
    {
        $this->repository->onlyTrashed();

        $book = $this->repository->find($id);

        return view('codeedubook::trashed.books.show', compact('book'));
    }


    public function update(Request $request, $id)
    {
        $this->repository->onlyTrashed();

        $this->repository->restore($id);

        $url = $request->get('redirect_to', route('trashed.books.index'));

        \Session::flash('message', 'Livro restaurado com sucesso!');
        
        return redirect()->to(\URL::previous());
    }
}
