<?php

namespace CodeEduBook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Routing\Controller;
use CodeEduBook\Http\Requests\BookCreateRequest;
use CodeEduBook\Http\Requests\BookUpdateRequest;
use CodeEduBook\Repositories\BookRepository;
use CodeEduBook\Repositories\CategoryRepository;
use CodeEduBook\Models\Book;
use CodeEduBook\Models\Category;
use CodePub\Criteria\FindByTitleCriteria;
use CodePub\Criteria\FindByAuthorCriteria;

class BooksController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $repository;

    protected $categoryRepository;

    public function __construct(BookRepository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;

        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $books = $this->repository->paginate(10);

        return view('codeedubook::books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories = $this->categoryRepository->lists('name', 'id'); //pluck

        return view('codeedubook::books.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(BookCreateRequest $request)
    {
        $data = $request->all();

        $data['author_id'] = \Auth::user()->id;

        $this->repository->create($data);
        // pegando a url anterior da requisição para redirecionar para a mesma    
        $url = $request->get('redirect_to', route('books.index'));
        // criando mensagens    
        $request->session()->flash('message', 'Livro cadastrado com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $book = $this->repository->find($id);

        $this->categoryRepository->withTrashed();

        $categories = $this->categoryRepository->listsWithMutators('name_trashed', 'id');

        return view('codeedubook::books.edit', compact('book', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(BookUpdateRequest $request, $id)
    {
        // retorna todos os dados exceto oque colocar nos parametros
        $data = $request->except(['author_id']);

        $this->repository->update($request->all(), $id);
        // pegando a url anterior da requisição para redirecionar para a mesma
        $url = $request->get('redirect_to', route('books.index'));

        $request->session()->flash('message', 'Livro alterado com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        \Session::flash('message', 'Livro excluído com sucesso!');
        
        return redirect()->to(\URL::previous());
    }
}
