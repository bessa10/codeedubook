<?php

namespace CodeEduBook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Routing\Controller;
use CodeEduBook\Models\Category;
use CodeEduBook\Http\Requests\CategoryRequest;
use CodeEduBook\Repositories\CategoryRepository;


class CategoriesController extends Controller
{

     /**
     * @var CategoryRepository
     */
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // fazendo a consulta e trazendo paginada
        // ACTIVE RECORD
        //$categories = Category::query()->paginate(10);
        // REPOSITORY
        $categories = $this->repository->paginate(10);

        return view('codeedubook::categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('codeedubook::categories.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CategoryRequest $request)
    {
        // Active record - criando
        //Category::create($request->all());
        $this->repository->create($request->all());
        // pegando a url anterior da requisição para redirecionar para a mesma    
        $url = $request->get('redirect_to', route('categories.index'));
        // criando mensagens    
        $request->session()->flash('message', 'Categoria cadastrada com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->repository->find($id);

        return view('codeedubook::categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CategoryRequest $request, $id)
    {
        // update active record
        //$category->fill($request->all());
        //$category->save();

        $this->repository->update($request->all(), $id);
        // pegando a url anterior da requisição para redirecionar para a mesma
        $url = $request->get('redirect_to', route('categories.index'));

        $request->session()->flash('message', 'Categoria alterada com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        // active record - delete
        //$category->delete();

        $this->repository->delete($id);

        \Session::flash('message', 'Categoria excluída com sucesso!');
        
        return redirect()->to(\URL::previous());
    }
}
